package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		boolean jugadaEncontrada = false;
		while(!jugadaEncontrada)
		{
			Random rnd = new Random();
			double elNumero = rnd.nextDouble();
			int elNumeroColumna = (int) (elNumero * tablero[0].length);
			
			for(int i = tablero.length - 1; i >= 0 && !jugadaEncontrada; i--)
			{
				if(tablero[i][elNumeroColumna] == "___")
				{
					tablero[i][elNumeroColumna] = elNumero + " ";
					jugadaEncontrada = true;
				}
			}
		}
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		boolean marcada = false;
		for(int i = tablero.length - 1; i >= 0 && !marcada; i--)
		{
			if(tablero[i][col] == "___")
			{
				marcada = true;
				String elSimbolo = jugadores.get(turno).darSimbolo();
				tablero[i][col] = elSimbolo;
			}
		}
		return marcada;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		
		return true;
	}
	
	public String darSimboloAtacante(String pNombre)
	{
		String elSimbolo = null;
		boolean loEncontro = false;
		for(int i = 0; i < jugadores.size() && !loEncontro; i++)
		{
			Jugador elJugador = (Jugador)jugadores.get(i);
			String elNombre = elJugador.darNombre();
			if(elNombre.equals(pNombre))
			{
				elSimbolo = elJugador.darSimbolo();
			}	
		}
		return elSimbolo;
	}



}
